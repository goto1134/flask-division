from flask import request, Flask

app = Flask(__name__)


@app.route('/divide')
def divide_route():
  a = float(request.args["a"])
  b = float(request.args["b"])
  return str(divide(a, b))


def divide(a, b):
  return a / b
