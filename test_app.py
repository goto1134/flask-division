from unittest import TestCase

from app import divide


class Test(TestCase):
  def test_divide_by_zero(self):
    with self.assertRaises(ZeroDivisionError):
      divide(1, 0)

  def test_division(self):
    self.assertEqual(divide(1, 2), 0.5)
